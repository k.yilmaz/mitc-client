
const role = {
  Management: 'Site Yönetimi',
  CompanyAdmin: 'Şirket Yönetimi',
  UserManagement: 'Kullanıcı Yönetimi',
  TemplateManagement: 'Template Yönetimi',
  ProjectManagement: 'Project Manager',
  HR: 'HR',
  FinanceManager: 'FinanceManager'
}

const permission = {
  DoAllProjectApprovement: 'DoAllProjectApprovement',
  DoAllProjectExpenseApprovement: 'DoAllProjectExpenseApprovement',
  DoAllProjectExpenseEntry: 'DoAllProjectExpenseEntry',
  DoAllProjectExpensePermission: 'DoAllProjectExpensePermission',
  DoAllProjectInvoice: 'DoAllProjectInvoice',
  DoAllProjectResource: 'DoAllProjectResource',
  DoAllProjectTimesheetPermission: 'DoAllProjectTimesheetPermission',
  DoAllProjectTimesheetEntryForUser: 'DoAllProjectTimesheetEntryForUser',
  DoAllProjectTimesheetEntry: 'DoAllProjectTimesheetEntry',
  DoClient: 'DoClient',
  DoCompanyExpense: 'DoCompanyExpense',
  DoInvoice: 'DoInvoice',
  DoProject: 'DoProject',
  DoProjectApprovement: 'DoProjectApprovement',
  DoProjectExpenseApprovement: 'DoProjectExpenseApprovement',
  DoProjectExpensePermission: 'DoProjectExpensePermission',
  DoProjectInvoice: 'DoProjectInvoice',
  DoProjectRequest: 'DoProjectRequest',
  DoProjectResource: 'DoProjectResource',
  DoProjectTimesheetPermission: 'DoProjectTimesheetPermission',
  DoResource: 'DoResource',
  DoResourceFinancial: 'DoResourceFinancial',
  DoSubCompany: 'DoSubCompany',
  DoTimePeriodClosing: 'DoTimePeriodClosing',
  DoUser: 'DoUser',
  ReportAllInvoice: 'ReportAllInvoice',
  ReportAllResource: 'ReportAllResource',
  ReportAllCompanyForecastIncome: 'ReportAllCompanyForecastIncome',
  ReportAllCompanyForecastCostCCI: 'ReportAllCompanyForecastCostCCI',
  ReportProjectForecastIncome: 'ReportProjectForecastIncome',
  ReportProjectForecastCostCCI: 'ReportProjectForecastCostCCI',
  ReportExpense: 'ReportExpense',
  ReportProjectExpense: 'ReportProjectExpense',
  ReportProjectInvoice: 'ReportProjectInvoice',
  ReportProjectResource: 'ReportProjectResource',
  ReportProjectTimesheet: 'ReportProjectTimesheet',
  ReportTimesheet: 'ReportTimesheet',
  SendAllProjectApprovement: 'SendAllProjectApprovement',
  SendProjectApprovement: 'SendProjectApprovement',
  ViewAllProjectForecastCostCCI: 'ViewAllProjectForecastCostCCI',
  ViewAllProjectForecastIncome: 'ViewAllProjectForecastIncome',
  ViewAllProjectInvoice: 'ViewAllProjectInvoice',
  ViewAllProjectResource: 'ViewAllProjectResource',
  ViewClient: 'ViewClient',
  ViewCompanyExpense: 'ViewCompanyExpense',
  ViewAllProjectExpense: 'ViewAllProjectExpense',
  ViewInvoice: 'ViewInvoice',
  ViewProject: 'ViewProject',
  ViewProjectForecastCostCCI: 'ViewProjectForecastCostCCI',
  ViewProjectForecastIncome: 'ViewProjectForecastIncome',
  ViewProjectInvoice: 'ViewProjectInvoice',
  ViewAllProjectRequest: 'ViewAllProjectRequest',
  ViewProjectResource: 'ViewProjectResource',
  ViewResource: 'ViewResource',
  ViewResourceFinancial: 'ViewResourceFinancial',
  ViewSubCompany: 'ViewSubCompany',
  ViewTimePeriodClosing: 'ViewTimePeriodClosing',
  ViewUser: 'ViewUser',
  DoSystemWbs: 'DoSystemWbs',
  DoFcForecastApprovement: 'DoFcForecastApprovement',
  ViewAllProjectDetails: 'ViewAllProjectDetails',
  ReportLateSubmission: 'ReportLateSubmission',
  TimesheetSettings: 'TimesheetSettings',
  DoPricing: 'DoPricing',
  ViewSystemWbs: 'ViewSystemWbs',
  DoProjectExpenseEntry: 'DoProjectExpenseEntry',
  DoProjectTimesheetEntry: 'DoProjectTimesheetEntry',
  ReportChargeability: 'ReportChargeability',
  DoAllProjectForecastIncome: 'DoAllProjectForecastIncome',
  DoAllProjectForecastCostCCI: 'DoAllProjectForecastCostCCI',
  DoProjectForecastIncome: 'DoProjectForecastIncome',
  DoProjectForecastCostCCI: 'DoProjectForecastCostCCI',
  ViewAllPricing: 'ViewAllPricing',
  DoAllPricing: 'DoAllPricing'
}

const language = {
  TR: 'tr',
  EN: 'en'
}

const logType = {
  ERROR: 4,
  WARNING: 2,
  INFO: 1
}

const timeAm = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
const timePm = [13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]

const UserExists = '11001'
const UserNotFoundInAd = '11002'

const GroupExists = '12001'
const GroupHasUser = '12002'

const Failed = '10001'

// File error messages,
const UPLOAD_FILE_FAILED = '20000'
const UPLOAD_FILE_PARTIAL_FAILED = '20001'
const UPLOAD_FILE_BULK_SUCCESS = '20002'
const UPLOAD_FILE_BULK_FAILED = '20003'

const RECORD_FOUND = '30000'
const RECORD_NOT_FOUND = '30001'
const EMAIL_FAILED = '40001'

const ActiveUserControl = {
  SetActiveUser: 'SetActiveUser',
  RemoveActiveUser: 'RemoveActiveUser'
}

const ReportType = {
  Payroll: 'Payroll',
  Invoice: 'Invoice',
  Expense: 'Expense'
}

const ForecastServiceFieldTypes = {
  Money: 1,
  Text: 2,
  Number: 3,
  Percent: 4
}

const GridFieldTypes = {
  Money: 'money',
  Text: 'text',
  Label: 'label',
  Number: 'number',
  Link: 'link',
  Percent: 'percent',
  IsDeleted: 'isDeleted'
}

const ProjectTypes = {
  ClientProject: 1,
  InternalProject: 2,
  BD: 3,
  Corporate: 4
  // Pricing: 5,
  // PricingBD: 6
}

const ProjectStatus = {
  Active: 1,
  Passive: 2,
  Draft: 3
}

const ScopeTypes = {
  Project: 1,
  Forecast: 2,
  Tenant: 3
}

const ExpenseFormStatus = {
  Reject: 2,
  SentForApproval: 3,
  Approved: 4,
  Draft: 1
}

const TimesheetStatus = {
  Draft: 1,
  Updated: 2,
  Adjusted: 3,
  Submitted: 4,
  Processed: 5
}

const ProjectAction = {
  ProjectCreation: 'ProjectCreation',
  ProjectRequest: 'ProjectRequest',
  ProjectUpdate: 'ProjectUpdate'
}

const ParameterType = {
  Level: 1,
  Billcode: 2,
  KDV: 3,
  ExpenseCostType: 4,
  ProjectType: 5,
  Currency: 6,
  InvoiceTaxRate: 7,
  ResourceWorkforce: 8,
  ResourceWorkplace: 9,
  ClientGroup: 10,
  CareerTrack: 11,
  PracticeArea: 12,
  InvoicePaymentType: 13,
  Status: 14,
  ProjectStatus: 15,
  PaymentStatus: 16,
  InvoiceStatus: 17,
  ClientStatus: 18,
  ToWEngagementType: 19,
  ProjectDocumentType: 20,
  NotificationType: 21,
  NotificationStatus: 22,
  NotificationTemplateType: 23,
  ProjectRequestStatus: 24,
  ExpenseFormStatus: 25,
  WbsType: 26,
  CommonWbsType: 27,
  TimesheetStatus: 28,
  LoadFactor: 29,
  TimesheetHolidayDayClose: 30,
  TimesheetHolidayType: 31,
  ModuleType: 32,
  ForecastApprovementStatus: 33,
  PricingStatus: 34,
  ProjectClientBilling: 35,
  ProjectServiceType: 36,
  InvoiceStopaj: 37,
  InvoiceTevkifat: 38
}

const ParameterGroup = {
  NullGroup: null,
  Zero: 0
}

const ExpenseWbs = {
  Masraf: 1,
  TimeSheet: 2
}

const InvoiceStatusType = {
  ProcessesRequested: 1, // Faturalama Bekliyor
  PaymentWaiting: 2,
  Paid: 3,
  Canceled: 4,
  Draft: 5,
  Proforma: 6, // Proforma Onay Bekliyor
  ProformaCreated: 7,
  InvoiceSentBack: 8,
  ProformaSentBack: 9
}

const InvoicePaymentType = {
  Fatura: 1,
  OnOdeme: 2,
  OnOdemeIstinaden: 3,
  KurFarki: 4
}

const TimesheetTimeType = {
  Hour: 1,
  Minute: 2
}

const DxTableChipDataIfFormat = {
  InvoiceTable: 1
}

const TimesheetHolidayDayClose = {
  AllDay: 1,
  HalfDay: 2
}

const NotificationStatus = {
  NotSent: 1,
  Sent: 2,
  Read: 3
}

const Modules = {
  Timesheet: 1,
  Expense: 2,
  Finance: 3
}

const PreClosureStatus = {
  Default: 0,
  Waiting: 1,
  Processing: 2,
  Processed: 3,
  Failed: 4
}

const InvoiceGridTypes = {
  Billed: '1',
  Description: '2',
  Level: '3',
  Rate: '4'
}

const ForecastApprovementStatus = {
  WaitingForApprove: 1,
  Approved: 2,
  Declined: 3,
  FCWaitingForApprove: 4,
  FCDeclined: 5
}

const ClientStatus = {
  Active: 1,
  Passive: 2
}

const ProjectReportType = {
  Finance: 1,
  Invoice: 2,
  FinanceResource: 3,
  FinanceExpense: 4,
  EnteredTimeSheetReport: 5,
  ReportChargeability: 6
}

const EnteredTimeSheetReportType = {
  TaskDetail: 1,
  MonthDetail: 2
}

const DocumentTemplateType = {
  InvoicePdf: 1
}

const ForecastGridType = {
  Revenues: 1,
  Billings: 2,
  Collections: 3,
  ChargeableExpenses: 4,
  TotalCost: 5,
  Payroll: 6,
  Expense: 7,
  Other: 8,
  SolutionCont: 9,
  CapitalCharge: 10,
  CCI: 11,
  CCIPerCent: 12
}

const ForecastHeaderType = {
  Actual: 1,
  Etc: 2,
  Eac: 3,
  Ode: 4,
  Delta: 5
}

const PricingStatusType = {
  Created: 1,
  ConvertedProject: 2
}

const PreClosureReportType = {
  Finance: 1,
  Invoice: 2,
  Resource: 3,
  Expense: 4
}

const ExpenseAccountingStatus = {
  Paid: 1,
  Completed: 2
}

export default {
  role: role,
  permission: permission,
  language: language,
  logType: logType,
  timeAm: timeAm,
  timePm: timePm,
  userExists: UserExists,
  userNotFoundInAd: UserNotFoundInAd,
  groupExists: GroupExists,
  groupHasUser: GroupHasUser,
  failed: Failed,
  UPLOAD_FILE_FAILED: UPLOAD_FILE_FAILED,
  UPLOAD_FILE_PARTIAL_FAILED: UPLOAD_FILE_PARTIAL_FAILED,
  UPLOAD_FILE_BULK_SUCCESS: UPLOAD_FILE_BULK_SUCCESS,
  UPLOAD_FILE_BULK_FAILED: UPLOAD_FILE_BULK_FAILED,
  RECORD_FOUND: RECORD_FOUND,
  RECORD_NOT_FOUND: RECORD_NOT_FOUND,
  EMAIL_FAILED: EMAIL_FAILED,
  activeUserControl: ActiveUserControl,
  reportType: ReportType,
  GridFieldTypes: GridFieldTypes,
  ForecastServiceFieldTypes: ForecastServiceFieldTypes,
  ProjectTypes: ProjectTypes,
  ProjectStatus: ProjectStatus,
  ScopeTypes: ScopeTypes,
  ExpenseFormStatus: ExpenseFormStatus,
  TimesheetStatus: TimesheetStatus,
  ProjectAction: ProjectAction,
  ParameterType: ParameterType,
  ParameterGroup: ParameterGroup,
  ExpenseWbs: ExpenseWbs,
  InvoiceStatusType: InvoiceStatusType,
  InvoicePaymentType: InvoicePaymentType,
  TimesheetTimeType: TimesheetTimeType,
  DxTableChipDataIfFormat: DxTableChipDataIfFormat,
  TimesheetHolidayDayClose: TimesheetHolidayDayClose,
  NotificationStatus: NotificationStatus,
  Modules: Modules,
  PreClosureStatus: PreClosureStatus,
  InvoiceGridTypes: InvoiceGridTypes,
  ForecastApprovementStatus: ForecastApprovementStatus,
  ClientStatus: ClientStatus,
  ProjectReportType: ProjectReportType,
  EnteredTimeSheetReportType: EnteredTimeSheetReportType,
  DocumentTemplateType: DocumentTemplateType,
  ForecastGridType: ForecastGridType,
  PricingStatusType: PricingStatusType,
  PreClosureReportType: PreClosureReportType,
  ForecastHeaderType: ForecastHeaderType,
  ExpenseAccountingStatus: ExpenseAccountingStatus
}
