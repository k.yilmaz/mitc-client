import Axios from 'axios'
import configStore from './configStore'
import router from '@/router/index'
import { AUTH_LOGOUT } from '@/store/actions/auth'
import { SET_OVERLAY } from '@/store/actions/overlay'
import store from '@/store'
import AuthHelper from '@/library/helpers/authhelper'

export default class ApiCall {
  baseUrl = '';

  constructor () {
    this.baseUrl = configStore.getApiUrl()

    // Create axios instance
    this.axios_instance = Axios.create({
      baseURL: this.baseUrl
    })

    // Activate request interceptor
    this.initRequestInterceptor()

    // Activate response interceptor
    this.initResponseInterceptor()
  }

  static getInstance () {
    if (!this._instance) this._instance = new ApiCall()

    return this._instance
  }

  // Request Interceptor
  initRequestInterceptor = () => {
    this.axios_instance.interceptors.request.use(function (config) {
      store.dispatch(SET_OVERLAY, true)

      const headers = {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }

      config.headers = { ...headers, ...config.headers }
      return config
    }, function (error) {
      return Promise.reject(error)
    })
  }

  // Response Interceptor
  initResponseInterceptor = () => {
    this.axios_instance.interceptors.response.use(
        (response) => this.handleResponse(response),
        (error) => this.handleResponseError(error)
    )
  }

  handleResponse = response => {
    store.dispatch(SET_OVERLAY, false)
    if (response && response.data && !response.data.IsSucceed && response.data.ErrorCode) {
      return Promise.reject(response.data)
    } else {
      return Promise.resolve(response)
    }
  }
  handleResponseError = error => {
    // eslint-disable-next-line no-debugger
    if (error?.response?.status === 401 && AuthHelper.GetAccessToken()) {
      return this.AxiosPost('/Authentication/RefreshAccessToken/', { refreshToken: AuthHelper.GetRefreshToken(), accessToken: AuthHelper.GetAccessToken() })
          .then((response) => {
            if (response.data.IsSucceed) {
              AuthHelper.SetAccessToken(response.data.Response.AccessToken)
              AuthHelper.SetRefreshToken(response.data.Response.RefreshToken)

              // Change Content-Type header
              error.config.headers.Authorization = 'Bearer ' + AuthHelper.GetAccessToken()

              // Return request object with axios
              return this.axios_instance(error.config)
            } else {
              store.dispatch(SET_OVERLAY, false)
              store.dispatch(AUTH_LOGOUT).then(() => {
                router.push('/login')
              })
            }
          })
          .catch(function () {
            store.dispatch(SET_OVERLAY, false)
            store.dispatch(AUTH_LOGOUT).then(() => {
              router.push('/login')
            })
            // error.response.data = { Response: [] }
            var returnError = { ErrorCode: '401' } // if koşulunda zaten 401 olduğu için direk yazılır
            return Promise.reject(returnError)
          })
    } else if (error?.response?.status === 401 && !AuthHelper.GetAccessToken()) {
      store.dispatch(SET_OVERLAY, false)
      router.push('/login')
      // error.response.data = { Response: [] }
      var returnError = { ErrorCode: '401' } // if koşulunda zaten 401 olduğu için direk yazılır
      return Promise.reject(returnError)
    } else {
      store.dispatch(SET_OVERLAY, false)
      // error.response.data = { ErrorCode: error.response.status }
      return Promise.reject(error.response.data)
    }
  }

  AxiosGet = async (url, headers) => await this.axios_instance.get(url, { headers: headers })

  AxiosGetWithPayload = async (url, payload, headers) => await this.axios_instance.get(url, payload, { headers: headers })

  AxiosPost = async (url, payload, headers) => await this.axios_instance.post(url, payload, { headers: headers })

  AxiosReadFile = async (url, payload, headers) => await this.axios_instance.post(url, payload, { headers: headers }, { responseType: 'arraybuffer' })

  AxiosPut = async (url, payload, headers) => await this.axios_instance.put(url, payload, { headers: headers })

  AxiosDelete = async (url, payload, headers) => await this.axios_instance.delete(url, payload, { headers: headers })
}
