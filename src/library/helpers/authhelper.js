import Converter from './converthelper'
const SecretKey = 'a2d'
export default class AuthHelper {
  static SetUser (value) {
    if (value) {
      const localItem = Converter.EncodeBase64FromString(JSON.stringify(value))
      localStorage.setItem('ui', SecretKey + localItem)
    }
  }

  static GetUser () {
    const localItem = localStorage.getItem('ui')
    if (localItem !== null) {
      return JSON.parse(Converter.DecodeBase64ToString(localItem.substr(3, localItem.length)))
    }
    return null
  }

  static RemoveUser () {
    localStorage.removeItem('ui')
    localStorage.removeItem('currentUser')
    localStorage.removeItem('userSelectedProject')
  }

  static SetAccessToken (value) {
    if (value) {
      localStorage.setItem('at', value)
    }
  }

  static GetAccessToken () {
    const localItem = localStorage.getItem('at')
    if (localItem !== null) {
      return localItem
    }
    return null
  }

  static RemoveAccessToken () {
    localStorage.removeItem('at')
  }

  static SetRefreshToken (value) {
    if (value) {
      localStorage.setItem('rt', value)
    }
  }

  static GetRefreshToken () {
    const localItem = localStorage.getItem('rt')
    if (localItem !== null) {
      return localItem
    }
    return null
  }

  static RemoveRefreshToken () {
    localStorage.removeItem('rt')
  }

  static IsAuthorized (keys) {
    // _ var ise permission, yok ise rol ü tanımlar.
    var user = this.GetUser()
    var roles = user.Roles
    var permission = user.Permissions
    var projectPermissions = user.ProjectPermissions

    if (Array.isArray(keys)) {
      var returnAuth = false
      keys.forEach(function (element) {
        if (element.startsWith('_')) {
          if (permission.includes(element.substr(1)) || projectPermissions.some(x => x.Permission === element.substr(1))) {
            returnAuth = true
          }
        } else {
          if (roles.includes(element)) {
            returnAuth = true
          }
        }
      })
    } else {
      if (keys.startsWith('_')) {
        if (permission.includes(keys.substr(1))) returnAuth = true
      } else {
        if (roles.includes(keys)) returnAuth = true
      }
    }
    return returnAuth
  }

  static AuthorizationCheckTenant (pagePermission) {
    if (this.GetUser()) {
      if (AuthHelper.GetUser().Permissions.includes(pagePermission)) {
        return true
      }
    }
    return false
  }

  static AuthorizationCheckProject (pagePermission, projectId) {
    if (this.GetUser()) {
      if (AuthHelper.GetUser().ProjectPermissions.some((x) => x.Permission === pagePermission && x.ProjectId === parseInt(projectId))) {
        return true
      }
    }
    return false
  }

  static AuthorizationCheckArray (arrayData) {
    if (this.GetUser()) {
      return (AuthHelper.GetUser().Permissions.some(r => arrayData.indexOf(r) >= 0) || AuthHelper.GetUser().ProjectPermissions.some(r => arrayData.indexOf(r.Permission) >= 0))
    }
    return false
  }

  static AuthorizationCheckPermission (pagePermission) {
    if (this.GetUser()) {
      var projectPermissions = AuthHelper.GetUser().ProjectPermissions.map(x => x.Permission)
      if (AuthHelper.GetUser().Permissions.includes(pagePermission) || projectPermissions.includes(pagePermission)) {
        return true
      } else {
        return false
      }
    }
    return false
  }
}
