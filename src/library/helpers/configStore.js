const configStore = {
  getApiUrl: function () {
    return process.env.VUE_APP_API_BASE_URL
  }
}

export default configStore
