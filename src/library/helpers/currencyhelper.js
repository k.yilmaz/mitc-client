import Forecasthelper from '@/library/helpers/forecasthelper'
export default class CurrencyHelper {
  static ParseLocalCurrency (st) {
    if (st.indexOf(',') === st.length - 3) {
      st = st.replace('.', '').replace(',', '.')
    } else {
      st = st.replace(',', '')
    }

    return parseFloat(st, 10)
  }

  static ForecastCurrency (x, y, z) {
    x = x.replaceAll('.', '').replaceAll(',', '.')
    y = y.replaceAll('.', '').replaceAll(',', '.')
    z = z.replaceAll('.', '').replaceAll(',', '.')
    var sonuc = (x * 10 + y * 10) / 10
    var sonuc2 = (sonuc * 10 - z * 10) / 10
    return parseFloat(sonuc2.toFixed(2))
  }

  static InvoiceCalculateAmountTax (invoiceModel) {
    if (invoiceModel.Tevkifat !== '-1' && invoiceModel.Stopaj !== '-1') { // Kdv + Stopaj + Tevkifat var
      const data = invoiceModel.Amount +
      ((invoiceModel.Amount / (1 - parseFloat(invoiceModel.Stopaj))) * (Forecasthelper.ReturnFloatFromMoney(invoiceModel.InvoiceTaxRate) / 100) * (1 - (parseFloat(invoiceModel.Tevkifat) / 100)))
      return data
    } else if (invoiceModel.Tevkifat !== '-1') { // Kdv + Tevkifat var
      const data = invoiceModel.Amount +
      (invoiceModel.Amount * Forecasthelper.ReturnFloatFromMoney(invoiceModel.InvoiceTaxRate) / 100 * (1 - (parseFloat(invoiceModel.Tevkifat) / 100)))
      return data
    } else if (invoiceModel.Stopaj !== '-1') { // Kdv + Stopaj var
      const data = invoiceModel.Amount +
      ((invoiceModel.Amount / (1 - parseFloat(invoiceModel.Stopaj)) * (Forecasthelper.ReturnFloatFromMoney(invoiceModel.InvoiceTaxRate) / 100)))
      return data
    } else { // Sadece Kdv var
      const data = invoiceModel.Amount +
      (invoiceModel.Amount * Forecasthelper.ReturnFloatFromMoney(invoiceModel.InvoiceTaxRate)) / 100
      return data
    }
  }
}
