import constants from '../constants/constants'

export default class Forecasthelper {
  isMoneyWithComma = false

  static GetForecastValueByPermission (doProjectForecast, viewProjectForecastIncome, viewProjectForecastCostCCI) {
    if (doProjectForecast || (viewProjectForecastIncome && viewProjectForecastCostCCI)) {
      return true
    }
    return false
  }

  static ConvertIntToMonthDotYear (mythis, date) {
    const monthArray = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec']
    return mythis.GetLabel(this, 'forecast_' + monthArray[Number(date.substring(date.length - 2)) - 1]) + '.' + date.substring(2, 4)
  }

  static GetTypeofField (element) {
    if (element.VariableType === constants.ForecastServiceFieldTypes.Money) {
      return constants.GridFieldTypes.Money
    } else if (element.VariableType === constants.ForecastServiceFieldTypes.Number) {
      return constants.GridFieldTypes.Number
    } else if (element.VariableType === constants.ForecastServiceFieldTypes.Text) {
      return constants.GridFieldTypes.Text
    } else if (element.VariableType === constants.ForecastServiceFieldTypes.Percent) {
      return constants.GridFieldTypes.Percent
    }
  }

  static GetValueofField (element) {
    if (element.VariableType === constants.ForecastServiceFieldTypes.Money) {
      if (this.isMoneyWithComma) {
        return new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(element.Value).toString().slice(0, -2)
      } else {
        return new Intl.NumberFormat('de-DE').format(element.Value)
      }
    } else if (element.VariableType === constants.ForecastServiceFieldTypes.Percent) {
      return element.Value.toString().concat('%')
    } else {
      return element.Value
    }
  }

  static GetFormattedMoney (_val) {
    if (!_val || _val === '') {
      return 0
    } else {
      if (this.isMoneyWithComma) {
        return new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(_val).toString().slice(0, -2)
      } else {
        return new Intl.NumberFormat('de-DE').format(_val)
      }
    }
  }

  static GetKValueofField (element, _val) {
    let value
    if (_val === null) {
      value = this.GetValueofField(element)
    } else {
      value = _val
    }
    if (value.includes('.')) {
      const elementDotArray = value.split('.')
      value = ''
      for (const [index, elem] of elementDotArray.entries()) {
        if (index === elementDotArray.length - 2) {
          if (Number(elementDotArray[elementDotArray.length - 1]) >= 500) {
            value = value.toString().concat((Number(elem) + 1).toString()).concat('K')
          } else {
            value = value.toString().concat(elem).concat('K')
          }
          break
        }
        value = value.toString().concat(elem).concat('.')
      }
    } else {
      value = (Number(value) / 1000).toString().concat('K')
    }

    return value
  }

  static GetMValueofField (element, _val) {
    let value
    if (_val === null) {
      value = this.GetValueofField(element)
    } else {
      value = _val
    }
    if (value.includes('.')) {
      const elementDotArray = value.split('.')
      value = ''
      if (elementDotArray.length === 2) {
        if (Number(elementDotArray[1]) >= 500) {
          value = value.toString().concat(((Number(elementDotArray[0]) + 1) / 1000).toString()).concat('M')
        } else {
          value = value.toString().concat((Number(elementDotArray[0]) / 1000).toString()).concat('M')
        }
      } else {
        for (const [index, elem] of elementDotArray.entries()) {
          if (index === elementDotArray.length - 3) {
            if (Number(elementDotArray[elementDotArray.length - 2]) >= 500) {
              value = value.toString().concat(((Number(elem) + 1)).toString()).concat('M')
            } else {
              value = value.toString().concat((Number(elem)).toString()).concat('M')
            }
            break
          }
          value = value.toString().concat(elem).concat('.')
        }
      }
    } else {
      value = (Number(value) / 1000000).toString().concat('M')
    }

    return value
  }

  static GetTotalValueofIndexedColumn (data, startIndex, endIndex, columnNumber, moneyType) {
    let result = 0
    for (let i = startIndex; i <= endIndex; i++) {
      result += this.ReturnFloatFromMoney(data[i][columnNumber].value)
    }

    result = new Intl.NumberFormat('de-DE').format(result)

    if (moneyType === 'M') {
      return this.GetMValueofField(null, result)
    } else if (moneyType === 'K') {
      return this.GetKValueofField(null, result)
    } else {
      return result
    }
  }

  static ReturnFloatFromMoney (value) {
    var thousandSeparator = Intl.NumberFormat('de').format(11111).replace(/\p{Number}/gu, '')
    var decimalSeparator = Intl.NumberFormat('de').format(1.1).replace(/\p{Number}/gu, '')

    if (value !== 0 && value !== '0' && value !== '0,00') {
      return parseFloat(value.toString()
        .replace(new RegExp('\\' + thousandSeparator, 'g'), '')
        .replace(new RegExp('\\' + decimalSeparator), '.')
      )
    } else {
      return 0
    }
  }

  // küsüratlar nokta şeklinde geliyor ama paraya çevirmede virgüllü string bir ifade kullanmak lazım.
  static ChangeDoubleDotToComma (value) {
    return value.replaceAll('.', ',')
  }

  static CalculateEAC (eventParams, data, eacIndex, oldValue, moneyType) {
    let diff = 0
    if (data[eventParams.Row][eventParams.Column].type === constants.GridFieldTypes.Money) {
      diff = this.ReturnFloatFromMoney(data[eventParams.Row][eventParams.Column].value) - this.ReturnFloatFromMoney(oldValue)
      diff += this.ReturnFloatFromMoney(data[eventParams.Row][eacIndex].value)
      diff = new Intl.NumberFormat('de-DE').format(diff)
      if (moneyType === 'M') {
        return this.GetMValueofField(null, diff)
      } else if (moneyType === 'K') {
        return this.GetKValueofField(null, diff)
      } else {
        return diff
      }
    } else if (data[eventParams.Row][eventParams.Column].type === constants.GridFieldTypes.Percent) {
      diff = parseFloat(data[eventParams.Row][eventParams.Column].value.toString().split('%')[0]) - parseFloat(oldValue.toString().split('%')[0])
      diff += parseFloat(data[eventParams.Row][eacIndex].value.toString().split('%')[0])
      return diff.toFixed(1).toString() + '%'
    } else {
      return diff
    }
  }

  static CalculateETC (eventParams, data, eacIndex, actualIndex, moneyType) {
    let diff = 0
    if (data[eventParams.Row][eventParams.Column].type === constants.GridFieldTypes.Money) {
      diff = this.ReturnFloatFromMoney(data[eventParams.Row][eacIndex].value) - this.ReturnFloatFromMoney(data[eventParams.Row][actualIndex].value)
      diff = new Intl.NumberFormat('de-DE').format(diff)
      if (moneyType === 'M') {
        return this.GetMValueofField(null, diff)
      } else if (moneyType === 'K') {
        return this.GetKValueofField(null, diff)
      } else {
        return diff
      }
    } else if (data[eventParams.Row][eventParams.Column].type === constants.GridFieldTypes.Percent) {
      diff = parseFloat(data[eventParams.Row][eacIndex].value.toString().split('%')[0]) - parseFloat(data[eventParams.Row][actualIndex].value.toString().split('%')[0])
      return diff.toFixed(1).toString() + '%'
    } else {
      return diff
    }
  }

  static CalculateDelta (type, eac, ode, moneyType, rowText) {
    if (type === constants.GridFieldTypes.Money) {
      var delta = 0
      if (rowText === constants.ForecastGridType.Payroll || rowText === constants.ForecastGridType.Expense || rowText === constants.ForecastGridType.Other || rowText === constants.ForecastGridType.SolutionCont || rowText === constants.ForecastGridType.CapitalCharge || rowText === constants.ForecastGridType.TotalCost) {
        delta = new Intl.NumberFormat('de-DE').format(this.ReturnFloatFromMoney(ode) - this.ReturnFloatFromMoney(eac))
      } else {
        delta = new Intl.NumberFormat('de-DE').format(this.ReturnFloatFromMoney(eac) - this.ReturnFloatFromMoney(ode))
      }
      if (moneyType === 'M') {
        return this.GetMValueofField(null, delta)
      } else if (moneyType === 'K') {
        return this.GetKValueofField(null, delta)
      } else {
        return delta
      }
    } else if (type === constants.GridFieldTypes.Percent) {
      return parseFloat(parseFloat(eac.toString().split('%')[0]) - parseFloat(ode.toString().split('%')[0]).toFixed(1).toString()).toFixed(1) + '%'
    } else {
      return 0
    }
  }

  static CalculateCCI (revenue, totalCost, moneyType) {
    let cci = 0
    let cciPercent = 0

    cci = this.ReturnFloatFromMoney(revenue) - this.ReturnFloatFromMoney(totalCost)

    if (cci === 0 || this.ReturnFloatFromMoney(revenue) === 0) { // 0' a bölme hatası için konuldu
      cciPercent = 0
    } else {
      cciPercent = (cci / this.ReturnFloatFromMoney(revenue)) * 100
    }

    cci = new Intl.NumberFormat('de-DE').format(cci)
    cciPercent = new Intl.NumberFormat('de-DE', { minimumFractionDigits: 1 }).format(cciPercent)

    if (moneyType === 'M') {
      return {
        CCI: this.GetMValueofField(null, cci),
        CCIPercent: cciPercent.toString() + '%'
      }
    } else if (moneyType === 'K') {
      return {
        CCI: this.GetKValueofField(null, cci),
        CCIPercent: cciPercent.toString() + '%'
      }
    } else {
      return {
        CCI: cci,
        CCIPercent: cciPercent.toString() + '%'
      }
    }
  }
}
