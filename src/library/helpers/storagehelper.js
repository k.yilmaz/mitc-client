import Converter from './converthelper'
export default class StorageHelper {
  static GetLocalStorageItem (item) {
    if (item) {
      const localItem = localStorage.getItem(item)
      return Converter.DecodeBase64ToString(localItem)
    }
  }

  static SetLocalStorageItem (item, value) {
    if (item) {
      const localValue = Converter.EncodeBase64FromString(value)
      localStorage.setItem(item, localValue)
    }
  }

  static RemoveLocalStorageItem (item) {
    if (item) {
      localStorage.removeItem(item)
    }
  }

  static GetLocalStorageObjectItem (item) {
    try {
      if (item) {
        const sessionValue = JSON.parse(decodeURIComponent(escape(window.atob(localStorage.getItem(item)))))
        return sessionValue
      }
    } catch {
      return undefined
    }
  }

  static SetLocalStorageObjectItem (item, value) {
    try {
      if (item && value) {
        this.RemoveLocalStorageItem(item)
        const sessionValue = this.base64EncodeUnicode(JSON.stringify(value))
        localStorage.setItem(item, sessionValue)
      }
    } catch (error) {
      console.error(error)
    }
  }

  static base64EncodeUnicode (str) {
    let utf8Bytes = ''
    utf8Bytes = encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function (match, p1) {
      return String.fromCharCode('0x' + p1)
    })
    return window.btoa(unescape(encodeURIComponent(utf8Bytes)))
  }
}
