
export default class StringHelper {
  static IsNullOrEmpty (val) {
    if (val === null || val === '' || val === 'undefined' || val === 'null' || val === undefined || val === '-1') {
      return true
    } else {
      return false
    }
  }

  static GetFormattedUsername (val) {
    try {
      if (val) {
        const splitedUsername = val.split('@')
        if (splitedUsername[0]) {
          return splitedUsername[0]
        } else {
          return val
        }
      } else {
        return val
      }
    } catch {
      return val
    }
  }

  static GetReplacedTurkishCharacters (val) {
    try {
      if (val) {
        const replacedVal = val
          .replaceAll('Ğ', 'G')
          .replaceAll('Ü', 'U')
          .replaceAll('Ş', 'S')
          .replaceAll('I', 'I')
          .replaceAll('İ', 'I')
          .replaceAll('Ö', 'O')
          .replaceAll('Ç', 'C')
          .replaceAll('ğ', 'g')
          .replaceAll('ü', 'u')
          .replaceAll('ş', 's')
          .replaceAll('ı', 'i')
          .replaceAll('ö', 'o')
          .replaceAll('ç', 'c')
        return replacedVal
      } else {
        return val
      }
    } catch {
      return val
    }
  }

  static turkishToLower (val) {
    var string = val
    var letters = { İ: 'i', I: 'ı', Ş: 'ş', Ğ: 'ğ', Ü: 'ü', Ö: 'ö', Ç: 'ç' }
    string = string.replace(/(([İIŞĞÜÇÖ]))+/g, function (letter) { return letters[letter] })
    return string.toLowerCase()
  }

  static turkishToUpper (val) {
    var string = val
    var letters = { i: 'İ', ş: 'Ş', ğ: 'Ğ', ü: 'Ü', ö: 'Ö', ç: 'Ç', ı: 'I' }
    string = string.replace(/(([iışğüçö]))+/g, function (letter) { return letters[letter] })
    return string.toUpperCase()
  }

  static contains (target, pattern) {
    var value = 0
    pattern.forEach(function (word) {
      value = value + target.includes(word)
    })
    return (value >= 1)
  }
}
