import Vue from 'vue'
import localStorageHelper from '@/library/helpers/storagehelper'

const dialogalertplugin = {
  install (Vue) {
    Vue.mixin({
      methods: {
        ShowDialogAlert (ctx, errorObject) {
          const tempLastErrorObject = localStorageHelper.GetLocalStorageObjectItem('_lerrobj')
          if (tempLastErrorObject) {
            if (tempLastErrorObject.ErrorCode !== errorObject.ErrorCode || (tempLastErrorObject.ErrorCode === errorObject.ErrorCode && new Date(tempLastErrorObject.LastDisplayDate) <= new Date(new Date().getTime() - 2000))) {
              ctx.$dialog.alert(
                ctx.GetErrorMessage(
                  ctx,
                  errorObject.ErrorCode,
                  errorObject.TransactionCode
                ),
                ctx.alertoptions
              )
              const lastErrorObject = {
                ErrorCode: errorObject.ErrorCode,
                LastDisplayDate: new Date()
              }
              localStorageHelper.SetLocalStorageObjectItem(
                '_lerrobj',
                lastErrorObject
              )
            }
          } else {
            const lastErrorObject = {
              ErrorCode: errorObject.ErrorCode,
              LastDisplayDate: new Date()
            }
            localStorageHelper.SetLocalStorageObjectItem(
              '_lerrobj',
              lastErrorObject
            )
            ctx.$dialog.alert(
              ctx.GetErrorMessage(
                ctx,
                errorObject.ErrorCode,
                errorObject.TransactionCode
              ),
              ctx.alertoptions
            )
          }
        }
      }
    })
  }
}

Vue.use(dialogalertplugin)

export default { dialogalertplugin }
