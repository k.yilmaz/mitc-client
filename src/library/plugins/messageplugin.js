import Vue from 'vue'

const messageplugin = {
  install (Vue) {
    Vue.mixin({
      methods: {
        GetMessage (ctx, message) {
          if (message) { return this.$t(message) } else { return message }
        },

        GetMessageWithData (ctx, message, val) {
          if (message) { return this.$t(message, { val: val }) } else { return message }
        },

        GetLabel (ctx, message) {
          if (message) { return this.$t('label_' + message) } else { return message }
        },

        GetLabelWithData (ctx, message, data) {
          if (message) { return data + ' ' + this.$t('label_' + message) } else { return message }
        },

        GetErrorMessage (ctx, code, transactionCode) {
          if (transactionCode) {
            if (code) { return this.$t('error_' + code) + '<br><p class="errorMessageStyle">' + this.$t('transaction_code') + transactionCode + '</p>' } else { return this.$t('error_500') }
          } else {
            if (code) {
              if (code === '423') { // Sayfaya girdiğinde modül açık,sayfadayken kapandiği zaman o sayfada işlem yapıyorsa eğer o sayfadan atıyoruz
                this.$router.push({
                  name: 'Home'
                })
                return this.$t('error_' + code)
              } else if (code === '401') { // Token hatasi ise istenirse ek işlem yapılabilir
                return this.$t('error_' + code)
              } else {
                return this.$t('error_' + code)
              }
            } else {
              return this.$t('error_500')
            }
          }
        }

      }
    })
  }
}

Vue.use(messageplugin)

export default { messageplugin }
