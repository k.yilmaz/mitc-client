import Vue from 'vue'

const stringplugin = {
  install (Vue, options) {
    Vue.mixin({
      methods: {
        TurkishCharacterToEnglish (text) {
          var letters = { İ: 'i', I: 'ı', Ş: 'ş', Ğ: 'ğ', Ü: 'ü', Ö: 'ö', Ç: 'ç' }
          this.string = text.replace(/(([İIŞĞÜÇÖ]))/g, function (letter) { return letters[letter] })
          this.string = this.string.toLowerCase().trim().replace(/\s/g, '')
          return this.string
        }
      }
    })
  }
}

Vue.use(stringplugin)

export default { stringplugin }
