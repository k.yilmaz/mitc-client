import { createApp } from 'vue'
import App from './App.vue'
import VueDatePicker from '@vuepic/vue-datepicker';
import '@vuepic/vue-datepicker/dist/main.css'

import vuetify from './plugins/vuetify'
import router from './router/index.js'
import store from './store'

const app = createApp(App)
app.component('VueDatePicker', VueDatePicker);
app.use(store)
app.use(router)
app.use(vuetify)
app.mount('#app')
