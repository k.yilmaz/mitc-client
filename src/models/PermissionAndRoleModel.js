export const Map = (permissions, id) => {
    if (id == 1) {
        const data = permissions.map(permission => ({
            Id: permission.Id,
            Name: permission.Name,
            Description: permission.Description,
            DisplayText: permission.DisplayText,
            IsActive: permission.IsActive == true ? "Aktif" : "Pasif",
            rol: "up"
        }));
        return data;
    } else {
        const data = permissions.map(permission => ({
            Id: permission.Id,
            Name: permission.Name,
            Description: permission.Description,
            DisplayText: permission.DisplayText,
            IsActive: permission.IsActive == true ? "Aktif" : "Pasif",
        }));
        return data;
    }

};