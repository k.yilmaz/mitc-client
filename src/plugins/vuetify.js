import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/lib/components'
import * as directives from 'vuetify/lib/directives'
import '@mdi/font/css/materialdesignicons.css'

const vuetify = createVuetify({
    components: {
      ...components,
    },
    directives: {
      ...directives,
    },
  })
export default vuetify;