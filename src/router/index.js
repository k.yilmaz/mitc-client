import {createWebHistory, createRouter} from "vue-router";
import store from '../store'


const routes = [
    {
        path: '/',
        component: import('@/views/EsgateView.vue'),
        meta: {
            authorize: []
        }
    },
    {
        path: '/login',
        component: import('@/views/LoginView.vue')
    },
    {
        path: '/user',
        component: import('@/views/user/UserView.vue'),
        meta: {
            authorize: ['Admin']
        }
    },
    {
        path: '/user/role',
        component: import('@/views/user/components/role/RoleView.vue'),
        meta: {
            authorize: ['Admin']
        }
    },
    {
        path: '/user/permission',
        component: import('@/views/user/components/permission/PermissionView.vue'),
        meta: {
            authorize: []
        }
    },
    {
        path: '/log',
        component: import('@/views/logger/LogView.vue'),
        meta: {
            authorize: []
        }
    },
    {
        path: '/reasuror',
        component: import('@/views/reasuror/ReasurorView.vue'),
        meta: {
            authorize: []
        }
    },
    {
        path: '/reference',
        component: import('@/views/reference/ReferenceView.vue'),
        meta: {
            authorize: []
        }
    },
    {
        path: '/trete',
        component: import('@/views/trete/TreteView.vue'),
        meta: {
            authorize: []
        }
    },
    {
        path: '/repository', component: import('@/views/repository/RepositoryView.vue'),
        meta: {
            authorize: []
        }
    }
]

const router = createRouter({
    history: createWebHistory(),
    routes,
});

router.beforeEach((to, from, next) => {
    const {authorize} = to.meta
    const currentUser = store.getters.currentUser
    if (authorize) {
        if (!currentUser) {
            return next({path: '/login', query: {returnUrl: to.path}})
        }
        if (authorize.length > 0 && !authorize.some(role => currentUser.Roles.includes(role))) {
            alert("Bu sayfaya yetkiniz bulunmamaktadır");
            return next({ path: '/' });
        }
    }
    next()
})
export default router;