import ApiCall from '../library/helpers/apicall';
import AuthHelper from '../library/helpers/authhelper'

export default class AuthService extends ApiCall {
  static Login = async (request) => await this.getInstance().AxiosPost('/Authentication/Login', request, null)

  static Register = async (request) => await this.getInstance().AxiosPost('/Authentication/Register', request, null)

  static ForgotPassword = async (request) => await this.getInstance().AxiosPost('/Authentication/ForgotPassword', request, null)

  static MailActivation = async (request) => await this.getInstance().AxiosPost('/Authentication/MailActivation', request, null)

  static ChangePassword = async (request) => await this.getInstance().AxiosPost('/Authentication/ChangePassword', request, null)

  static ChangePasswordByOldPasword = async (request) => await this.getInstance().AxiosPost('/Authentication/ChangePasswordByOldPassword', request, null)

  static Logout = async () => await this.getInstance().AxiosPost('/Authentication/Logout', {}, { Authorization: 'Bearer ' + AuthHelper.GetAccessToken() })

  static IsActiveVerificationLink = async (request) => await this.getInstance().AxiosPost('/Authentication/IsActiveVerificationLink', request, null)

  static IsLogin = async () => await this.getInstance().AxiosPost('/Authentication/IsLogin', {}, { Authorization: 'Bearer ' + AuthHelper.GetAccessToken() })
}
