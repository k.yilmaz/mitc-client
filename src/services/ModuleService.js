import ApiCall from '../library/helpers/apicall'
import AuthHelper from '../library/helpers/authhelper'

export default class ModuleService extends ApiCall {
  static GetModules = async () => await this.getInstance().AxiosGet('management/Module/GetModules', { Authorization: 'Bearer ' + AuthHelper.GetAccessToken() })

  static GetModulesWithoutPermission = async () => await this.getInstance().AxiosGet('management/Module/GetModulesWithoutPermission', { Authorization: 'Bearer ' + AuthHelper.GetAccessToken() })

  static CreateModule = async (request) => await this.getInstance().AxiosPost('management/Module/CreateModule', request, { Authorization: 'Bearer ' + AuthHelper.GetAccessToken() })

  static UpdateModule = async (request) => await this.getInstance().AxiosPost('management/Module/UpdateModule', request, { Authorization: 'Bearer ' + AuthHelper.GetAccessToken() })
}
