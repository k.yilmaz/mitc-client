import ApiCall from '../library/helpers/apicall'
import AuthHelper from '@/library/helpers/authhelper'

export default class ParameterService extends ApiCall {
  static GetParameters = async (request) => await this.getInstance().AxiosPost('management/Parameter/GetParameters', request, { Authorization: 'Bearer ' + AuthHelper.GetAccessToken() })

  static GetTimePeriodsList = async () => await this.getInstance().AxiosGet('Finance/Forecast/GetTimePeriods', { Authorization: 'Bearer ' + AuthHelper.GetAccessToken() })

  static GetActiveTimePeriod = async () => await this.getInstance().AxiosGet('finance/Forecast/GetActiveTimePeriod', { Authorization: 'Bearer ' + AuthHelper.GetAccessToken() })

  static GetProjectStatusList = async () => await this.getInstance().AxiosPost('management/Parameter/ParametersByType', { ParameterType: 15 }, { Authorization: 'Bearer ' + AuthHelper.GetAccessToken() })
}
