import ApiCall from '../library/helpers/apicall'
import AuthHelper from '@/library/helpers/authhelper'


export default class PermissionService extends ApiCall {
  static GetAllPermission = async () => await this.getInstance().AxiosGet('/Permission/GetAllPermission', { Authorization: 'Bearer ' + AuthHelper.GetAccessToken() });
  static AddPermission = async (request) => await this.getInstance().AxiosPost('/Permission/AddPermission', request, { Authorization: 'Bearer ' + AuthHelper.GetAccessToken() });
  static UpdatePermission = async (request) => await this.getInstance().AxiosPost('/Permission/UpdatePermission', request, { Authorization: 'Bearer ' + AuthHelper.GetAccessToken() });
  static DeletePermission = async (id) => await this.getInstance().AxiosDelete('/Permission/DeletePermission', +id, { Authorization: 'Bearer ' + AuthHelper.GetAccessToken() });




}
