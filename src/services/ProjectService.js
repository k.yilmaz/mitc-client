import ApiCall from '../library/helpers/apicall'
import AuthHelper from '@/library/helpers/authhelper'

export default class ProjectService extends ApiCall {
  static GetUserProjectsRoles = async (id) => await this.getInstance().AxiosGet('management/Project/GetUserProjectsRoles/' + id, { Authorization: 'Bearer ' + AuthHelper.GetAccessToken() })

  static GetProjectsWithClient = async () => await this.getInstance().AxiosGet('management/Project/GetProjectsWithClient', { Authorization: 'Bearer ' + AuthHelper.GetAccessToken() })
}
