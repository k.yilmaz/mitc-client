import ApiCall from '../library/helpers/apicall'
import AuthHelper from '@/library/helpers/authhelper'

export default class RoleService extends ApiCall {

    static GetAllRoles = async () => await this.getInstance().AxiosGet('/Role/GetAllRoles',  {Authorization: 'Bearer ' + AuthHelper.GetAccessToken()});
    static AddRole = async (request) => await this.getInstance().AxiosPost('/Role/AddRole', request, {Authorization: 'Bearer ' + AuthHelper.GetAccessToken()});
    static UpdateRole = async (request) => await this.getInstance().AxiosPut('/Role/UpdateRole', request, {Authorization: 'Bearer ' + AuthHelper.GetAccessToken()});
    static DeleteRole = async (id) => await this.getInstance().AxiosDelete('/Role/DeleteRole', +id, {Authorization: 'Bearer ' + AuthHelper.GetAccessToken()});
    
    static GetRolePermissionByRoleId = async () => await this.getInstance().AxiosGet('/Role/GetRolePermissionByRoleId',  {Authorization: 'Bearer ' + AuthHelper.GetAccessToken()});
    static AddRolePermission = async (request) => await this.getInstance().AxiosPost('/Role/AddRolePermission', request, {Authorization: 'Bearer ' + AuthHelper.GetAccessToken()});
    static UpdateRolePermission = async (request) => await this.getInstance().AxiosPut('/Role/UpdateRolePermission', request, {Authorization: 'Bearer ' + AuthHelper.GetAccessToken()});

    static GetUserRoleByUserId = async () => await this.getInstance().AxiosGet('/Role/GetUserRoleByUserId',  {Authorization: 'Bearer ' + AuthHelper.GetAccessToken()});
    static AddUserRole = async (request) => await this.getInstance().AxiosPost('/Role/AddUserRole', request, {Authorization: 'Bearer ' + AuthHelper.GetAccessToken()});
    static UpdateUserRole = async (request) => await this.getInstance().AxiosPut('/Role/UpdateUserRole', request, {Authorization: 'Bearer ' + AuthHelper.GetAccessToken()});
 }
