import ApiCall from '@/library/helpers/apicall';
import AuthHelper from '@/library/helpers/authhelper';
export default class UserService extends ApiCall {
    static GetAllUser = async () => await this.getInstance().AxiosGet('/User/GetAllUser',  {Authorization: 'Bearer ' + AuthHelper.GetAccessToken()});
    static GetUserByUserName = async (username) => await this.getInstance().AxiosGet(`/User/GetUserByUserName?UseName=${username}`, {Authorization: 'Bearer ' + AuthHelper.GetAccessToken()});
    static GetUsersStartWith = async (request) => await this.getInstance().AxiosPost('/User/UsernameByUsers', request, {Authorization: 'Bearer ' + AuthHelper.GetAccessToken()});
    static GetUsersStartByEnterpriseName = async (request) => await this.getInstance().AxiosPost('/User/EnterpriseNameByUsers', request, {Authorization: 'Bearer ' + AuthHelper.GetAccessToken()});
    static GetAllPrivileges = async () => await this.getInstance().AxiosGet('/User/Privilege', {Authorization: 'Bearer ' + AuthHelper.GetAccessToken()});
    static GetUserPrivileges = async (id) => await this.getInstance().AxiosPost('/User/GetUserByPrivileges/' + id, {}, {Authorization: 'Bearer ' + AuthHelper.GetAccessToken()});
    static CreateUser = async (request) => await this.getInstance().AxiosPost('/User/CreateUser', request, {Authorization: 'Bearer ' + AuthHelper.GetAccessToken()});
    static UpdateUser = async (request) => await this.getInstance().AxiosPost('/User/UpdateUserIdByUserMetaData', request, {Authorization: 'Bearer ' + AuthHelper.GetAccessToken()});
}


