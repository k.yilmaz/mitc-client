import { createStore } from 'vuex'
import auth from './modules/auth'
import user from './modules/user'
import parameter from './modules/parameter'
import overlay from './modules/overlay'
import module from './modules/module'

export default createStore({
  modules: {
    auth,
    user,
    parameter,
    overlay,
    module
  }
})
