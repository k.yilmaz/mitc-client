import {
  AUTH_REQUEST,
  AUTH_ERROR,
  AUTH_SUCCESS,
  AUTH_LOGOUT,
  REGISTER_REQUEST,
  REGISTER_ERROR,
  FORGOT_REQUEST,
  ACTIVATION_REQUEST,
  CHANGE_PASSWORD_REQUEST,
  CHANGE_PASSWORD_BY_OLD_PASSWORD_REQUEST,
  IS_ACTIVE_VERIFICATION_LINK,
  AUTH_ISLOGIN
} from '../actions/auth'
import AuthService from '@/services/AuthService'
import AuthHelper from '@/library/helpers/authhelper'

const state = {
  currentUser: AuthHelper.GetUser() || '',
  accesstoken: AuthHelper.GetAccessToken() || '',
  refreshtoken: AuthHelper.GetRefreshToken() || ''
}

const getters = {
  isAuthenticated: state => !!state.accesstoken,
  currentUser: state => state.currentUser
}
const actions = {
  [AUTH_REQUEST]: ({ commit }, request) => {
    return new Promise((resolve, reject) => {
      AuthService.Login(request).then(response => {
        AuthHelper.SetUser({ UserId: response.data.Response.Id, UserName: response.data.Response.Username, Roles: response.data.Response.Roles, Permissions: response.data.Response.Permissions, ProjectPermissions: response.data.Response.ProjectPermissions })
        AuthHelper.SetAccessToken(response.data.Response.AccessToken)
        AuthHelper.SetRefreshToken(response.data.Response.RefreshToken)
        commit(AUTH_SUCCESS, response)
        return resolve(response)
      })
        .catch(error => {
          AuthHelper.RemoveUser()
          AuthHelper.RemoveAccessToken()
          AuthHelper.RemoveRefreshToken()
          commit(AUTH_ERROR)
          return reject(error)
        })
    })
  },
  [AUTH_LOGOUT]: ({ commit }) => {
    return new Promise((resolve) => {
      AuthService.Logout()
      commit(AUTH_LOGOUT)
      AuthHelper.RemoveUser()
      AuthHelper.RemoveAccessToken()
      AuthHelper.RemoveRefreshToken()
      return resolve()
    })
  },
  // eslint-disable-next-line no-unused-vars
  [REGISTER_REQUEST]: ({ commit }, request) => {
    return new Promise((resolve, reject) => {
      AuthService.Register(request).then(response => {
        return resolve(response)
      })
        .catch(error => {
          return reject(error)
        })
    })
  },
  // eslint-disable-next-line no-unused-vars
  [FORGOT_REQUEST]: ({ commit }, request) => {
    return new Promise((resolve, reject) => {
      AuthService.ForgotPassword(request).then(response => {
        return resolve(response)
      })
        .catch(error => {
          return reject(error)
        })
    })
  },
  // eslint-disable-next-line no-unused-vars
  [ACTIVATION_REQUEST]: ({ commit }, request) => {
    return new Promise((resolve, reject) => {
      AuthService.MailActivation(request).then(response => {
        return resolve(response)
      })
        .catch(error => {
          return reject(error)
        })
    })
  },
  // eslint-disable-next-line no-unused-vars
  [CHANGE_PASSWORD_REQUEST]: ({ commit }, request) => {
    return new Promise((resolve, reject) => {
      AuthService.ChangePassword(request).then(response => {
        return resolve(response)
      })
        .catch(error => {
          return reject(error)
        })
    })
  },
  // eslint-disable-next-line no-unused-vars
  [CHANGE_PASSWORD_BY_OLD_PASSWORD_REQUEST]: ({ commit }, request) => {
    return new Promise((resolve, reject) => {
      AuthService.ChangePasswordByOldPasword(request).then(response => {
        return resolve(response)
      })
        .catch(error => {
          return reject(error)
        })
    })
  },
  // eslint-disable-next-line no-unused-vars
  [IS_ACTIVE_VERIFICATION_LINK]: ({ commit }, request) => {
    return new Promise((resolve, reject) => {
      AuthService.IsActiveVerificationLink(request).then(response => {
        return resolve(response)
      })
        .catch(error => {
          return reject(error)
        })
    })
  },
  [AUTH_ISLOGIN]: () => {
    return new Promise(resolve => {
      AuthService.IsLogin()
      resolve()
    })
  }
}

const mutations = {
  [AUTH_SUCCESS]: (state, response) => {
    state.currentUser = { UserId: response.data.Response.Id, UserName: response.data.Response.Username, Roles: response.data.Response.Roles, Permissions: response.data.Response.Permissions }
    state.accesstoken = response.data.Response.AccessToken
    state.refreshtoken = response.data.Response.RefreshToken
  },
  [AUTH_ERROR]: state => {
    state.currentUser = null
    state.accesstoken = null
    state.refreshtoken = null
  },
  [AUTH_LOGOUT]: state => {
    state.currentUser = ''
    state.accesstoken = ''
    state.refreshtoken = ''
  },
  [REGISTER_REQUEST]: (state) => {
    state.currentUser = ''
    state.accesstoken = ''
    state.refreshtoken = ''
  },
  [REGISTER_ERROR]: state => {
    state.currentUser = null
    state.accesstoken = null
    state.refreshtoken = null
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
