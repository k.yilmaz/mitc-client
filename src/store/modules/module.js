import { LIST_MODULES, CREATE_MODULE, UPDATE_MODULE, CHECK_MODULES } from '../actions/module'
import { LIST_PARAMETER } from '../actions/parameter'

import constants from '@/library/constants/constants'
import ModuleService from '@/services/ModuleService'

import store from '../../store'

const state = {
  modules: []
}

const getters = {
  modules: state => state.modules
}

const actions = {
  [LIST_MODULES]: ({ commit }) => {
    return new Promise((resolve) => {
      const moduleList = []
      const request = {
        Type: constants.ParameterType.ModuleType,
        Group: constants.ParameterGroup.NullGroup
      }

      store.dispatch(LIST_PARAMETER, request).then(() => {
        ModuleService.GetModules().then((response) => {
          store.getters.parameterValues.forEach(element => {
            const module = response.data.Response.find(x => x.Type === Number(element.Code))
            moduleList.push({ Name: element.Value, Type: Number(element.Code), IsOpen: module?.IsOpen ?? true, IsExist: !!module })
          })
          commit(LIST_MODULES, moduleList)

          return resolve()
        })
          .catch((errorObject) => {
            this.$dialog.alert(
              this.GetErrorMessage(this, errorObject.ErrorCode, errorObject.TransactionCode),
              this.alertoptions
            )
          })
      })
        .catch((errorObject) => {
          this.$dialog.alert(
            this.GetErrorMessage(this, errorObject.ErrorCode, errorObject.TransactionCode),
            this.alertoptions
          )
        })
    })
  },
  [CHECK_MODULES]: ({ commit }) => {
    return new Promise((resolve) => {
      const moduleList = []
      const request = {
        Type: constants.ParameterType.ModuleType,
        Group: constants.ParameterGroup.NullGroup
      }

      store.dispatch(LIST_PARAMETER, request).then(() => {
        ModuleService.GetModulesWithoutPermission().then((response) => {
          store.getters.parameterValues.forEach(element => {
            const module = response.data.Response.find(x => x.Type === Number(element.Code))
            moduleList.push({ Name: element.Value, Type: Number(element.Code), IsOpen: module?.IsOpen ?? true, IsExist: !!module })
          })
          commit(CHECK_MODULES, moduleList)

          return resolve()
        })
          .catch((errorObject) => {
            this.$dialog.alert(
              this.GetErrorMessage(this, errorObject.ErrorCode, errorObject.TransactionCode),
              this.alertoptions
            )
          })
      })
        .catch((errorObject) => {
          this.$dialog.alert(
            this.GetErrorMessage(this, errorObject.ErrorCode, errorObject.TransactionCode),
            this.alertoptions
          )
        })
    })
  },
  [CREATE_MODULE]: ({ commit }, request) => {
    return new Promise((resolve, reject) => {
      ModuleService.CreateModule(request).then(response => {
        commit(CREATE_MODULE, request)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  [UPDATE_MODULE]: ({ commit }, request) => {
    return new Promise((resolve, reject) => {
      ModuleService.UpdateModule(request).then(response => {
        commit(UPDATE_MODULE, request)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  }
}

const mutations = {
  [LIST_MODULES]: (state, response) => {
    state.modules = response
  },
  [CHECK_MODULES]: (state, response) => {
    state.modules = response
  },
  [CREATE_MODULE]: (state, request) => {
    var index = state.modules.findIndex(x => x.Type === request.Type)
    state.modules[index].IsOpen = request.IsOpen
    state.modules[index].IsExist = true
  },
  [UPDATE_MODULE]: (state, request) => {
    var index = state.modules.findIndex(x => x.Type === request.Type)
    state.modules[index].IsOpen = request.IsOpen
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
