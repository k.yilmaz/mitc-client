import { SET_OVERLAY } from '../actions/overlay'

const state = {
  loadingOverlay: false
}

const getters = {
  loadingOverlay: state => state.loadingOverlay
}

const actions = {
  [SET_OVERLAY]: ({ commit }, value) => {
    commit(SET_OVERLAY, value)
  }
}

const mutations = {
  [SET_OVERLAY]: (state, value) => {
    state.loadingOverlay = value
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
