import { LIST_PARAMETER, LIST_TIME_PERIODS, GET_ACTIVE_PERIOD, LIST_CURRENCIES, LIST_TAXES, LIST_MASRAF_TYPES, LIST_CLIENT_GROUPS, LIST_PROJECT_TYPES, LIST_PROJECT_STATUS, LIST_INVOICES_STATUS, LIST_INVOICES_TYPE, LIST_CURRENCY, LIST_INVOICE_TAX_RATE, LIST_CLIENT_STATUS, LIST_ENGAGEMENT_TYPE, LIST_DOCUMENT_TYPE, LIST_EXPENSE_FORM_STATUS } from '../actions/parameter'
import ParameterService from '../../services/ParameterService'

const state = {
  parameterValues: [],
  timePeriods: [],
  activePeriod: null,
  currencies: [],
  taxes: [],
  masrafTypes: [],
  clientGroups: [],
  levels: [],
  workForces: [],
  workPlaces: [],
  careerTracks: [],
  practiceAreas: [],
  projectTypes: [],
  projectStatus: [],
  invoicesStatus: [],
  invoicesTypes: [],
  currencyList: [],
  invoiceTaxRates: [],
  clientStatus: [],
  engagementTypeList: [],
  documentTypeList: [],
  expenseFormStatus: []
}

const getters = {
  parameterValues: state => state.parameterValues,
  timePeriods: state => state.timePeriods,
  activePeriod: state => state.activePeriod,
  currencies: state => state.currencies,
  taxes: state => state.taxes,
  masrafTypes: state => state.masrafTypes,
  levels: state => state.levels,
  workForces: state => state.workForces,
  workPlaces: state => state.workPlaces,
  careerTracks: state => state.careerTracks,
  practiceAreas: state => state.practiceAreas,
  projectTypes: state => state.projectTypes,
  projectStatus: state => state.projectStatus,
  invoicesStatus: state => state.invoicesStatus,
  invoicesTypes: state => state.invoicesTypes,
  currencyList: state => state.currencyList,
  invoiceTaxRates: state => state.invoiceTaxRates,
  clientStatus: state => state.clientStatus,
  engagementTypeList: state => state.engagementTypeList,
  documentTypeList: state => state.documentTypeList,
  expenseFormStatus: state => state.expenseFormStatus
}

const actions = {
  [LIST_PARAMETER]: ({ commit }, request) => {
    return new Promise((resolve, reject) => {
      ParameterService.GetParameters(request).then(response => {
        commit(LIST_PARAMETER, response)
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },
  [LIST_TIME_PERIODS]: ({ commit }) => {
    return new Promise((resolve, reject) => {
      ParameterService.GetTimePeriodsList().then(response => {
        commit(LIST_TIME_PERIODS, response)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  [GET_ACTIVE_PERIOD]: ({ commit }) => {
    return new Promise((resolve, reject) => {
      ParameterService.GetActiveTimePeriod().then(response => {
        commit(GET_ACTIVE_PERIOD, response)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  [LIST_CURRENCIES]: ({ commit }) => {
    return new Promise((resolve, reject) => {
      ParameterService.GetCurrencyList().then(response => {
        commit(LIST_CURRENCIES, response)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  [LIST_TAXES]: ({ commit }) => {
    return new Promise((resolve, reject) => {
      ParameterService.GetTaxList().then(response => {
        commit(LIST_TAXES, response)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  [LIST_MASRAF_TYPES]: ({ commit }) => {
    return new Promise((resolve, reject) => {
      ParameterService.GetMasrafTypeList().then(response => {
        commit(LIST_MASRAF_TYPES, response)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  [LIST_CLIENT_GROUPS]: ({ commit }) => {
    return new Promise((resolve, reject) => {
      ParameterService.GetClientGroup().then(response => {
        commit(LIST_CLIENT_GROUPS, response)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  [LIST_PROJECT_TYPES]: ({ commit }) => {
    return new Promise((resolve, reject) => {
      ParameterService.GetProjectTypeList().then(response => {
        commit(LIST_PROJECT_TYPES, response)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  [LIST_PROJECT_STATUS]: ({ commit }) => {
    return new Promise((resolve, reject) => {
      ParameterService.GetProjectStatusList().then(response => {
        commit(LIST_PROJECT_STATUS, response)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  [LIST_INVOICES_STATUS]: ({ commit }) => {
    return new Promise((resolve, reject) => {
      ParameterService.GetInvoiceStatus().then(response => {
        commit(LIST_INVOICES_STATUS, response)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  [LIST_INVOICES_TYPE]: ({ commit }) => {
    return new Promise((resolve, reject) => {
      ParameterService.GetInvoiceType().then(response => {
        commit(LIST_INVOICES_TYPE, response)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  [LIST_CURRENCY]: ({ commit }) => {
    return new Promise((resolve, reject) => {
      ParameterService.GetCurrencyList().then(response => {
        commit(LIST_CURRENCY, response)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  [LIST_INVOICE_TAX_RATE]: ({ commit }) => {
    return new Promise((resolve, reject) => {
      ParameterService.GetInvoiceTaxRateList().then(response => {
        commit(LIST_INVOICE_TAX_RATE, response)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  [LIST_CLIENT_STATUS]: ({ commit }) => {
    return new Promise((resolve, reject) => {
      ParameterService.GetClientStatusList().then(response => {
        commit(LIST_CLIENT_STATUS, response)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  [LIST_ENGAGEMENT_TYPE]: ({ commit }) => {
    return new Promise((resolve, reject) => {
      ParameterService.GetEngagementTypeList().then(response => {
        commit(LIST_ENGAGEMENT_TYPE, response)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  [LIST_EXPENSE_FORM_STATUS]: ({ commit }) => {
    return new Promise((resolve, reject) => {
      ParameterService.GetExpenseFormStatus().then(response => {
        commit(LIST_EXPENSE_FORM_STATUS, response)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  [LIST_DOCUMENT_TYPE]: ({ commit }) => {
    return new Promise((resolve, reject) => {
      ParameterService.GetDocumentTypeList().then(response => {
        commit(LIST_DOCUMENT_TYPE, response)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  }
}

const mutations = {
  [LIST_PARAMETER]: (state, response) => {
    state.parameterValues = response.data.Response
  },
  [LIST_TIME_PERIODS]: (state, response) => {
    state.timePeriods = response.data.Response
  },
  [GET_ACTIVE_PERIOD]: (state, response) => {
    state.activePeriod = response.data.Response
  },
  [LIST_CURRENCIES]: (state, response) => {
    state.currencies = response.data.Response
  },
  [LIST_TAXES]: (state, response) => {
    state.taxes = response.data.Response
  },
  [LIST_MASRAF_TYPES]: (state, response) => {
    state.masrafTypes = response.data.Response
  },
  [LIST_CLIENT_GROUPS]: (state, response) => {
    state.clientGroups = response.data.Response
  },
  [LIST_PROJECT_TYPES]: (state, response) => {
    state.projectTypes = response.data.Response
  },
  [LIST_PROJECT_STATUS]: (state, response) => {
    state.projectStatus = response.data.Response
  },
  [LIST_INVOICES_STATUS]: (state, response) => {
    state.invoicesStatus = response.data.Response
  },
  [LIST_INVOICES_TYPE]: (state, response) => {
    state.invoicesTypes = response.data.Response
  },
  [LIST_CURRENCY]: (state, response) => {
    state.currencyList = response.data.Response
  },
  [LIST_INVOICE_TAX_RATE]: (state, response) => {
    state.invoiceTaxRates = response.data.Response
  },
  [LIST_CLIENT_STATUS]: (state, response) => {
    state.clientStatus = response.data.Response
  },
  [LIST_ENGAGEMENT_TYPE]: (state, response) => {
    state.engagementTypeList = response.data.Response
  },
  [LIST_DOCUMENT_TYPE]: (state, response) => {
    state.documentTypeList = response.data.Response
  },
  [LIST_EXPENSE_FORM_STATUS]: (state, response) => {
    state.expenseFormStatus = response.data.Response
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
