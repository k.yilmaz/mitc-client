import { LIST_PRIVILEGES } from '../actions/user'
import UserService from '../../services/UserService'

const state = {
  users: [],
  privileges: []
}

const getters = {
  users: state => state.users,
  privileges: state => state.privileges
}

const actions = {
  [LIST_PRIVILEGES]: ({ commit }) => {
    return new Promise((resolve, reject) => {
      UserService.GetAllPrivileges().then(response => {
        commit(LIST_PRIVILEGES, response)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  }
}

const mutations = {
  [LIST_PRIVILEGES]: (state, response) => {
    state.privileges = response.data.Response
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
