import CryptoJS from 'crypto-js';

// Şifreleme anahtarını ve iv'yi belirle
const ENCRYPTION_KEY = 'BuBirOrnekSifrelemeAnahtari12345';
const ENCRYPTION_IV = 'BuBirOrnekSifrelemeIV6789';

// Listeyi şifreleyip Local Storage'a kaydeden fonksiyon
const setEncryptedList = (list,keyList) => {
  try {
    // Listeyi JSON formatına dönüştür
    const listJson = JSON.stringify(list);
    
    // Şifreleme için kullanılacak anahtarı ve iv'yi belirle
    const key = CryptoJS.enc.Utf8.parse(ENCRYPTION_KEY);
    const iv = CryptoJS.enc.Utf8.parse(ENCRYPTION_IV);
    
    // Listeyi AES şifreleme yöntemi ile şifrele
    const encryptedList = CryptoJS.AES.encrypt(listJson, key, { iv: iv }).toString();
    
    // Şifrelenmiş listeyi Local Storage'a kaydet
    localStorage.setItem(keyList, encryptedList);
  } catch (error) {
    console.error('Liste şifrelenirken bir hata oluştu: ', error);
  }
};

// Local Storage'dan şifreyi çözüp liste verisini döndüren fonksiyon
const getDecryptedList = (keyList) => {
  try {
    // Local Storage'dan şifrelenmiş listeyi al
    const encryptedList = localStorage.getItem(keyList);
    
    if (encryptedList) {
      // Şifreleme için kullanılacak anahtarı ve iv'yi belirle
      const key = CryptoJS.enc.Utf8.parse(ENCRYPTION_KEY);
      const iv = CryptoJS.enc.Utf8.parse(ENCRYPTION_IV);
      
      // Şifrelenmiş listeyi AES şifre çözme yöntemi ile çöz
      const bytes = CryptoJS.AES.decrypt(encryptedList, key, { iv: iv });
      
      // Çözülen veriyi UTF-8 formatına dönüştür ve parse et
      const decryptedList = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
      
      // Çözülen listeyi döndür
      return decryptedList;
    } else {
      // Eğer şifrelenmiş liste yoksa veya boşsa, null döndür
      return null;
    }
  } catch (error) {
    console.error('Liste çözülürken bir hata oluştu: ', error);
    return null;
  }
};

export{setEncryptedList,getDecryptedList}