export const Validation = {
    nameRule: [
        v => !!v || 'Name is required',
    ],
    usernameRules:[
        v => (v.length > 10) || 'Name must be less than 10 characters',
    ],
    emailRules: [
        v => /^\w+([.-]?\w+)@\w+([.-]?\w+)(\.\w{2,3})+$/.test(v) || 'E-mail must be valid'
    ]
};